<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profil', 'HomeController@profil')->name('profil');

Route::group(['middleware' => 'auth'], function () {

	// Route::get('/abstract/create', 'UploadAbstractController@create')->name('abstract.create');
	Route::post('/abstract/store', 'UploadAbstractController@store')->name('abstract.store');
  Route::get('/abstract/delete', 'UploadAbstractController@delete')->name('abstract.delete');
  Route::get('/abstract/update/{id}', 'UploadAbstractController@submit')->name('abstract.submit');

  //Add Memeber Author
  Route::get('/author/member/add-new', 'AuthorController@create')->name('author.create');
  Route::get('/author/member/show/{id}', 'AuthorController@showAnggota')->name('profil.show');
  Route::get('/author/member/edit/{id}', 'AuthorController@editAnggota')->name('profil.edit');
  Route::get('/author/member/delete/{id}', 'AuthorController@deleteAnggota')->name('profil.delete');
  Route::post('/author/member/update/{id}', 'AuthorController@updateAnggota')->name('profil.update');
  Route::post('/author/member/store', 'AuthorController@store')->name('author.store');

  //AUthor Submit Abstract & paper
  Route::get('/author/submission', 'AuthorController@index')->name('submission.index');
  Route::get('/author/submission/{id}', 'AuthorController@show')->name('submission.detail');
  Route::get('/author/abstract/delete/{id}', 'UploadAbstractController@delete')->name('abstract.delete');
  Route::post('/author/abstract/store', 'UploadAbstractController@store')->name('abstract.store');

  Route::get('/author/payment', 'AuthorController@payment')->name('pay.index');
  Route::post('/author/payment/submit', 'AuthorController@payment_submit')->name('pay.submit');
  Route::post('/author/payment/upload', 'AuthorController@confirm_pay')->name('pay.upload');

});

Route::group(['middleware' => 'author'], function() {

    // Route::get('/author/profil', 'AuthorController@profil')->name('author.profil');
    Route::get('/author/full-paper/upload', 'AuthorController@upload')->name('author.upload');
    Route::post('/author/full-paper/set-upload', 'AuthorController@set_upload')->name('author.set_upload');

    // Route::get('/author/payment', 'AuthorController@payment')->name('pay.index');
    // Route::post('/author/payment/submit', 'AuthorController@payment_submit')->name('pay.submit');
    // Route::post('/author/payment/upload', 'AuthorController@confirm_pay')->name('pay.upload');
});

Route::group(['middleware' => 'reviewer'], function() {
    Route::get('/reviewer/abstrak', 'ReviewerController@abstrak')->name('review.abstrak');
    Route::get('/home/abstract/approved/action/{id}', 'VerifController@actionApprove')->name('verif.approved.action');
    Route::get('/home/abstract/reject/action/{id}', 'VerifController@actionReject')->name('verif.reject.action');
});

Route::group(['middleware' => ['verificator'||'reviewer'||'admin']], function () {
    // Route::get('/home/abstract/reject/action/{id}', 'VerifController@actionReject')->name('verif.reject.action');
    Route::get('/home/abstract/show/{id}', 'VerifController@show')->name('verif.show');
});

Route::group(['middleware' => ['verificator'||'admin']], function () {

    Route::get('/home/abstract', 'VerifController@index')->name('verif.index');
    Route::get('/home/abstract/unverif', 'VerifController@unverif')->name('verif.unverif');
    Route::get('/home/abstract/approved', 'VerifController@approved')->name('verif.approved');
    Route::get('/home/abstract/reject', 'VerifController@reject')->name('verif.reject');
    // Route::get('/home/abstract/show/{id}', 'VerifController@show')->name('verif.show');
    Route::get('/home/abstract/approved/action/{id}', 'VerifController@actionApprove')->name('verif.approved.action');
    Route::get('/home/abstract/reject/action/{id}', 'VerifController@actionReject')->name('verif.reject.action');

    Route::get('/home/user', 'VerifController@user')->name('verif.user');
    Route::get('/home/user/{id}', 'VerifController@userDetail')->name('verif.user.detail');
    Route::get('/home/user-not-upload-abstrak', 'VerifController@user_belum_abstrak')->name('verif.user.notupload');
    Route::get('/home/user-not-submit-abstrak', 'VerifController@user_belum_submit')->name('verif.user.notsubmit');

    Route::get('/home/payment', 'VerifController@payment')->name('verif.payment');
    Route::get('/home/payment/confirm/{id}', 'VerifController@payment_confirm')->name('verif.payment.confirm');
    Route::get('/home/payment/invoice/{id}', 'VerifController@payment_invoice')->name('verif.payment.invoice');
    Route::get('/home/payment/info/{id}', 'VerifController@payment_info')->name('verif.payment.info');
    Route::get('/home/payment/history', 'VerifController@payment_history')->name('verif.payment.history');

    Route::get('/report', 'VerifController@report');
    Route::post('/tiket/submit', 'VerifController@submitMessage')->name('tiket');

});

// Route::get('/reviewer/abstrak', 'ReviewerController@abstrak')->name('review.abstrak');
Auth::routes();
Route::get('/login/reviewer', 'Auth\LoginController@reviewer');

Route::post('/change_password/{id}', 'HomeController@changePassword')->name('change.password');
Route::get('/test', 'HomeController@test');

route::get('/email/dump', 'HomeController@dump_email');
