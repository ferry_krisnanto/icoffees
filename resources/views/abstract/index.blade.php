@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif



	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-primary">
					<div class="panel-heading">Notification Paper</div>
					<div class="panel-body">
						@foreach($tiket as $message)

							<div class="direct-chat-text">
	            	<p>{{ $message->pesan }}</p>
								@if(!empty($message->file))
								<p>File Attach : <a href="{{url('/tiket/'.$message->file)}}" class="btn-small btn-warning">download</a></p>
								@endif
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Abstract</div>

					<div align="center" class="panel-body">

						@if($abstract == null)
						<h4>Submitted abstract was Closed by System, You only Register as a Participants</h4>
						 <h4>Anda Belum Upload Abstract</h4>
						<!--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">-->
						<!--  Upload Abstract-->
						<!--</button>-->

						<!-- Modal -->
						<div align="left" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="myModalLabel">Upload Abstract</h4>
						      </div>
						      <div class="modal-body">
										<form method="post" action="{{route('abstract.store')}}" enctype="multipart/form-data">
											{{ csrf_field() }}
											<div class="form-group">
												<input type="hidden" name="id_user" value="{{Auth::user()->id}}">
												<label for="subTheme">Abstract Title</label>
												<input type="text" class="form-control" id="abstractTitle" placeholder="Abstract Title" name="abstactTitle" required>
											</div>
											<div class="form-group">
												<label for="subTheme">Sub Theme</label>
												<select class="form-control" name="subTheme" required>
													<option value="">-- Select Sub Theme --</option>
												<option value="1">Democracy and Election</option>
												<option value="2">Environmental and Natural Resources</option>
												<option value="3">Modern Society and Human Security</option>
												<option value="4">Business and Economic Rights</option>
												<option value="5">Individual and Social Justice</option>
												<option value="6">Good Governance and Public Service</option>
												<option value="7">Indigenous Rights</option>
												<option value="8">Woman and Children</option>
											</select>
											</div>
											<div class="form-group">
												<label for="fileAbstract">File input</label>
												<input type="file" id="fileAbstract" name="fileAbstract" required>
												<p class="help-block">Upload File PDF/DOCX, Maks file size upload 2Mb</p>
											</div>
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-info">Submit</button>
										</form>
						      </div>
						    </div>
						  </div>
						</div>
						@else

						@if($abstract->status == 1)
						<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">×</span>
										<span class="sr-only">Close</span>
								</button>
								<strong>Please check your Abstract before submitting. If your abstract is correct please press submit-abstrct button below.</strong>
								<p>Note : When You submitted it, It can't be changed.</p>
						</div>
						@endif
						<div align="left">
						<form class="form-horizontal">
						  <div class="form-group">
						    <label class="col-sm-3 control-label"><strong>Abstract Title :</strong></label>
						    <div class="col-sm-9">
						      <p class="form-control-static">{{$abstract->title}}</p>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="col-sm-3 control-label"><strong>Sub Theme :</strong></label>
								<div class="col-sm-9">
									@if( $abstract->sub_theme == 1 )
									<p class="form-control-static">Democracy and Election</p>
									@elseif($abstract->sub_theme == 2)
									<p class="form-control-static">Environmental and Natural Resources</p>
									@elseif($abstract->sub_theme == 3)
									<p class="form-control-static">Modern Society and Human Security</p>
									@elseif($abstract->sub_theme == 4)
									<p class="form-control-static">Business and Economic Rights</p>
									@elseif($abstract->sub_theme == 5)
									<p class="form-control-static">Individual and Social Justice</p>
									@elseif($abstract->sub_theme == 6)
									<p class="form-control-static">Good Governance and Public Service</p>
									@elseif($abstract->sub_theme == 7)
									<p class="form-control-static">Indigenous Rights</p>
									@elseif($abstract->sub_theme == 8)
									<p class="form-control-static">Woman and Children</p>
									@endif
						    </div>
						  </div>
							@if($abstract->status == 1)
							<div class="form-group">
						    <label class="col-sm-3 control-label"><strong>Abstract :</strong></label>
						    <div class="col-sm-9">
						      <p class="form-control-static">
											<a href="{{url('/file/'.$abstract->file_name)}}" class="btn btn-primary">Download/Preview Abstract</a>
									</p>
						    </div>
						  </div>
							@elseif($abstract->status == 2)
							<div class="form-group">
						    <label class="col-sm-3 control-label"><strong>Status Abstract :</strong></label>
						    <div class="col-sm-9">
						      <p class="form-control-static">
											<button class="btn btn-default">Abstract On Process</button>
											<a href="{{url('/file/'.$abstract->file_name)}}" class="btn btn-primary">Download Abstract</a>
									</p>
						    </div>
						  </div>
							@elseif($abstract->status == 3)
							<div class="form-group">
						    <label class="col-sm-3 control-label"><strong>Status Abstract :</strong></label>
						    <div class="col-sm-9">
						      <p class="form-control-static">
										<button class="btn btn-success">Abstract Approved</button>
										<a href="{{url('/file/'.$abstract->file_name)}}" class="btn btn-primary">Download Abstract</a>
									</p>
						    </div>
						  </div>
							@elseif($abstract->status == 0)
							<div class="form-group">
						    <label class="col-sm-3 control-label"><strong>Status Abstract :</strong></label>
						    <div class="col-sm-9">
						      <p class="form-control-static">
										<button class="btn btn-warning">Abstract Ditolak</button>
									</p>
						    </div>
						  </div>
							@endif
						</form>
					</div>


						<!-- <iframe id="iframepdf" src="{{asset('file/coba.docx')}}"></iframe> -->
						<iframe src="https://docs.google.com/gview?url={{asset('/file/'. $abstract->file_name)}}" style="width:800px; height:400px;" frameborder="0"></iframe>
						@if($abstract->status == 1)
						</br>
						<div aligin="center">
								<a href="{{route('abstract.submit', $abstract->id)}}" class="btn btn-success">Submit - Abstract</a>
								<a href="{{route('abstract.delete', $abstract->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Re-Upload or Delete</a>
						</div>
						@endif

						<!-- khusus admin -->
						@if(Auth::user()->id_role > 10 &&  $abstract->status == 2)
						<div aligin="center">
							<a href="{{route('verif.approved.action', $abstract->id)}}" class="btn btn-success" onclick="return confirm('Are you sure you want to Approved this Abstract?');">Approved</a>
							<a href="{{route('verif.reject.action', $abstract->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to Reject this Abstract?');">Reject</a>
						</div>
						@elseif(Auth::user()->id_role == 2)
						<div aligin="center">
							<!-- <a href="{{route('verif.approved.action', $abstract->id)}}" class="btn btn-success" onclick="return confirm('Are you sure you want to Approved this Abstract?');">Approved</a>
							<a href="{{route('verif.reject.action', $abstract->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to Reject this Abstract?');">Reject</a> -->
							<button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#ticket">
							  Cteate Notification
							</button>
						</div>
						@endif


						@endif
						<!-- <div id="disqus_thread"></div>

						<script>

						/**
						*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
						*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
						/*
						var disqus_config = function () {
						this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
						this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
						};
						*/
						(function() { // DON'T EDIT BELOW THIS LINE
						var d = document, s = d.createElement('script');
						s.src = 'https://oals.disqus.com/embed.js';
						s.setAttribute('data-timestamp', +new Date());
						(d.head || d.body).appendChild(s);
						})();
						</script>
						<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

					</div> -->
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="ticket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				<!-- Modals -->
				<form action="{{route('tiket')}}" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group">
						<!-- <label for="exampleInputEmail1">Email address</label> -->
						<input type="hidden" class="form-control" value="{{$abstract->id_user or ''}}" name="id_user" required>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Pesan</label>
						<textarea class="form-control" rows="5" name="pesan" required></textarea>
						<!-- <input type="password" class="form-control" rows="5" placeholder="Password"> -->
					</div>
					<div class="form-group">
						<label for="exampleInputFile">File input</label>
						<input type="file" name="file">
						<!-- <p class="help-block">Example block-level help text here.</p> -->
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
