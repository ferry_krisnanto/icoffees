@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Daftar Abstract - {{$name or 'Semua'}}</div>

					<div class="panel-body">
						<table id="example" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>E-mail</th>
									<th>Abstract Title</th>
									<th>Sub Theme</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@php
								$i = 1;
								@endphp

								@foreach($datas as $data)
								<tr>
									<th scope="row">{{$i++}}</th>
									<td>{{$data->name}}</td>
									<td>{{$data->email}}</td>
									<td>{{$data->title}}</td>
									<td>
										@if( $data->sub_theme == 1 )
										Democracy and Election
										@elseif($data->sub_theme == 2)
										Environmental and Natural Resources
										@elseif($data->sub_theme == 3)
										Modern Society and Human Security
										@elseif($data->sub_theme == 4)
										Business and Economic Rights
										@elseif($data->sub_theme == 5)
										Individual and Social Justice
										@elseif($data->sub_theme == 6)
										Good Governance and Public Service
										@elseif($data->sub_theme == 7)
										Indigenous Rights
										@elseif($data->sub_theme == 8)
										Woman and Children
										@endif
									</td>
									<td>
										@if($data->status == 2)
										<button class="btn btn-default">Unverified</button>
										@elseif($data->status == 3 )
										<button class="btn btn-success">Approved</button>
										@elseif($data->status == 0 )
										<button class="btn btn-danger">Reject</button>
										@endif
									</td>
									<td><a href="{{route('submission.detail', $data->id_user)}}" class="btn btn-primary">Detail</a>
									<!-- <a href="">Show</a></td> -->
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
