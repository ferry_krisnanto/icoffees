@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">REPORT DATA</div>

					<div class="panel-body">
            <table id="example" class="table table-striped table-bordered">
              <thead>
                <tr>
									<th>#</th>
									<th>Judul Abstrak</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Universitas</th>
                  <th>Departmen/Faculty</th>
                  <th>Phone</th>
                  <th>Attending As</th>
                  <th>Dinner</th>
                  <th>Tour</th>
									<th>Hotel</th>
                </tr>
              </thead>
              <tbody>
								<?php $i = 1; ?>
                @foreach($user as $data)
                <tr>
									<th>{{$i++}}</th>
									<td>{{ $data->title }}</td>
                  <td>{{ $data->nama }}</td>
                  <td>
										@if(empty($data->email))
											{{$data->emailuser}}
										@else
											{{$data->email}}
										@endif
									</td>
                  <td>{{ $data->universitas }}</td>
                  <td>{{ $data->fakultas }}</td>
                  <td>{{ $data->phone }}</td>
                  <td>
                    <?php $member = $data; ?>
                    @if($member->attend == 1)
                    Attending Conference as Presenter
                    @elseif($member->attend == 2)
                    Attending Conference as Public/Magister/Doctor (Non-Presenter)
                    @elseif($member->attend == 3)
                    Attending Conference as Students (Non-Presenter)
                    @elseif($member->attend == 4)
                    Not Attending Conference
                    @endif
                  </td>
                  <td>{{$data->dinner == 0 ? 'No' : 'Yes'}}</td>
                  <td>{{$data->tour == 0 ? 'No' : 'Yes'}}</td>
                  <td>{{$data->hotel == 1 ? 'Yes' : 'No'}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
