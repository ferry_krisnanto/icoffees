@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif



	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Abstract</div>

					<div class="panel-body">
            <table id="example" class="table table-striped table-bordered">
              <thead>
                <tr>
									<th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Role</th>
									<th>Action</th>
                </tr>
              </thead>
              <tbody>
								<?php $i = 1; ?>
                @foreach($user as $data)
                <tr>
									<th>{{$i++}}</th>
                  <td>{{ $data->name }}</td>
                  <td>{{ $data->email }}</td>
                  <td>
										@if($data->id_role == 1)
										<span type="button" class="btn btn-xs bg-orange btn-flat margin">Admin</span>
										@elseif ($data->id_role ==2)
										<span type="button" class="btn btn-xs bg-purple btn-flat margin">Verificator</span>
										@elseif ($data->id_role ==3)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Author</span>
										@elseif ($data->id_role ==11)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 1</span>
										@elseif ($data->id_role ==12)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 2</span>
										@elseif ($data->id_role ==13)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 3</span>
										@elseif ($data->id_role ==14)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 4</span>
										@elseif ($data->id_role ==15)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 5</span>
										@elseif ($data->id_role ==16)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 6</span>
										@elseif ($data->id_role ==17)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 7</span>
										@elseif ($data->id_role ==18)
										<span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 8</span>
										@else
										<span type="button" class="btn btn-xs bg-maroon btn-flat margin">User</span>
										@endif
									</td>
									<td>
										<a href="{{route('verif.user.detail', $data->id)}}" class="btn btn-info">Detail</a>
									</td>
                </tr>
                @endforeach
              </tbody>
            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
