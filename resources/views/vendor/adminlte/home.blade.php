@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<!-- <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">Home</div>

					<div class="panel-body">
						{{ trans('adminlte_lang::message.logged') }}
					</div>
				</div>
			</div> -->
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel panel-primary">
						<div class="panel-heading">Notification Message</div>
						<div class="panel-body">
							@foreach($tiket as $message)

								<div class="direct-chat-text">
		            	<p>{{ $message->pesan }}</p>
									@if(!empty($message->file))
									<p>File Attach : <a href="{{url('/tiket/'.$message->file)}}" class="btn-small btn-warning">download</a></p>
									@endif
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-primary">
					<div class="panel-heading">Announcement</div>
					<div class="panel-body">

						<ul class="timeline">
	            <!-- timeline time label -->
	            <!-- <li class="time-label">
	                  <span class="bg-red">
	                    10 Feb. 2014
	                  </span>
	            </li> -->
	            <!-- /.timeline-label -->
	            <!-- timeline item -->
	            <!-- <li>
	              <i class="fa fa-envelope bg-blue"></i>

	              <div class="timeline-item">
	                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

	                <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

	                <div class="timeline-body">
	                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
	                  weebly ning heekya handango imeem plugg dopplr jibjab, movity
	                  jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
	                  quora plaxo ideeli hulu weebly balihoo...
	                </div>
	                <div class="timeline-footer">
	                  <a class="btn btn-primary btn-xs">Read more</a>
	                  <a class="btn btn-danger btn-xs">Delete</a>
	                </div>
	              </div>
	            </li>
	            <li>
	              <i class="fa fa-user bg-aqua"></i>

	              <div class="timeline-item">
	                <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

	                <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request</h3>
	              </div>
	            </li> -->
	            <!-- END timeline item -->
	            <!-- timeline item -->
							<li class="time-label">
	                  <span class="bg-green">
	                     9 August. 2018
	                  </span>
	            </li>
	            <!-- /.timeline-label -->
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-user bg-red"></i>

	              <div class="timeline-item">
	                <h3 class="timeline-header"><a href="#">Deadline Attendance List</a> For Presenter</h3>
	                <div class="timeline-body">
	                  <!-- <div class="embed-responsive embed-responsive-16by9">
	                    <iframe class="embed-responsive-item" src="#" frameborder="0" allowfullscreen=""></iframe>
	                  </div> -->
	                </div>
	                <div class="timeline-footer">
										Please complete the data author, and Attendance List for Presenter Before 9, August 2018. Because after that, the system will be auto closed.
	                  <a href="{{route('profil')}}" class="btn btn-xs bg-blue">Profil</a>
	                </div>
	              </div>
	            </li>


							<li class="time-label">
	                  <span class="bg-green">
	                     25 July. 2018
	                  </span>
	            </li>
	            <!-- /.timeline-label -->
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-bullhorn bg-blue"></i>

	              <div class="timeline-item">
	                <h3 class="timeline-header"><a href="#">Abstract Notification</a> Announcement</h3>
	                <div class="timeline-body">
	                  <!-- <div class="embed-responsive embed-responsive-16by9">
	                    <iframe class="embed-responsive-item" src="#" frameborder="0" allowfullscreen=""></iframe>
	                  </div> -->
	                </div>
	                <div class="timeline-footer">
										Check Your Abstract Notification on this link
	                  <a href="{{route('submission.index')}}" class="btn btn-xs bg-maroon">Abstract Notification</a>
										<br>
										<p>After Check your notification, please complete the Data Information and Attendance List on Profil Menu</p>
	                </div>
	              </div>
	            </li>

	            <li>
	              <i class="fa fa-comments bg-yellow"></i>

	              <div class="timeline-item">
	                <span class="time"><i class="fa fa-clock-o"></i> 30th June 2018</span>

	                <h3 class="timeline-header"><a href="#">Sytem Update</a> Version 2.0</h3>

	                <div class="timeline-body">
										<i style="color:red;">*</i> New User Interface & Design</br>
										<i style="color:red;">*</i> Menu Attendance of Conference</br>
	                  <i style="color:red;">*</i> Menu Payment</br>
	                </div>
	                <div class="timeline-footer">
	                  <!-- <a href="" class="btn btn-warning btn-flat btn-xs">Download Guide</a> -->
	                </div>
	              </div>
	            </li>

							<li>
	              <i class="fa fa-user bg-yellow"></i>
	              <div class="timeline-item">
	                <!--<span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>-->

	                <h3 class="timeline-header"><a href="#">New</a> Information</h3>

	                <div class="timeline-body">
	                  Registration as a <strong>participant only</strong> of conference, deadline 31 august 2018.
	                </div>
	                <div class="timeline-footer">
	                  <!--<a class="btn btn-warning btn-flat btn-xs">View comment</a>-->
	                </div>
	              </div>
	            </li>
	            <!-- END timeline item -->
	            <!-- timeline time label -->
							<li class="time-label">
	                  <span class="bg-green">
	                     10 July. 2018
	                  </span>
	            </li>
	            <!-- /.timeline-label -->
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-bullhorn bg-blue"></i>

	              <div class="timeline-item">
	                <h3 class="timeline-header"><a href="#">Abstract Submission</a> Deadline Extended</h3>
	                <div class="timeline-body">
	                  <!-- <div class="embed-responsive embed-responsive-16by9">
	                    <iframe class="embed-responsive-item" src="#" frameborder="0" allowfullscreen=""></iframe>
	                  </div> -->
	                </div>
	                <div class="timeline-footer">
										Open Registration &
	                  <a href="{{route('submission.index')}}" class="btn btn-xs bg-maroon">Submit Abstract</a>
	                </div>
	              </div>
	            </li>

	            <li class="time-label">
	                  <span class="bg-green">
	                     30 June. 2018
	                  </span>
	            </li>
	            <!-- /.timeline-label -->
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-video-camera bg-maroon"></i>

	              <div class="timeline-item">
	                <h3 class="timeline-header"><a href="#">Abstract Submission</a> Deadline</h3>
	                <div class="timeline-body">
	                  <!-- <div class="embed-responsive embed-responsive-16by9">
	                    <iframe class="embed-responsive-item" src="#" frameborder="0" allowfullscreen=""></iframe>
	                  </div> -->
	                </div>
	                <div class="timeline-footer">
										Open Registration &
	                  <a href="{{route('submission.index')}}" class="btn btn-xs bg-maroon">Submit Abstract</a>
	                </div>
	              </div>
	            </li>
	            <!-- END timeline item -->
	            <li>
	              <i class="fa fa-clock-o bg-gray"></i>
	            </li>
	          </ul>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
