<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="http://i-coffees.id/">International Conference on Fundamental Rights</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="#">I-Coffees.id</a>.</strong>
</footer>
