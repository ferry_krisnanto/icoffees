<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="http://i.coffees.fh.unila.ac.id/wp-content/uploads/2018/05/I-COFFEES-Logo-300x300.png" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url('home') }}"><i class='fa fa-home'></i> <span>{{ trans('adminlte_lang::message.home') }}</span></a></li>
            <li><a href="{{ url('profil') }}"><i class='fa fa-user'></i> <span>Profil</span></a></li>

            <!-- For User Biasa  -->
            @if(Auth::user()->id_role == 0)
            <li><a href="{{route('submission.index')}}"><i class='fa fa-file-text'></i> <span>Abstract Submission</span></a></li>
            <!--<li><a href="{{route('pay.index')}}"><i class='fa fa-file-text'></i> <span>Payment</span></a></li>-->

            <!-- For Admin & Verificator -->
            @elseif(Auth::user()->id_role == 2 || Auth::user()->id_role == 1)

            <!-- For Admin Only -->
            @if(Auth::user()->id_role == 1)
            <li><a href="{{route('submission.index')}}"><i class='fa fa-file-text'></i> <span>Abstract/Paper Submission</span></a></li>
            @endif

            <li class="treeview">
                <a href="#"><i class='fa fa-file-text'></i> <span>User List</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('verif.user') }}"><i class='fa fa-file-text'></i> <span>All User</span></a></li>
                    <li><a href="{{ route('verif.user.notupload')}}"><i class='fa fa-file-text'></i> <span>Not yet Upload</span></a></li>
                    <li><a href="{{ route('verif.user.notsubmit')}}"><i class='fa fa-file-text'></i> <span>Not yet Submit</span></a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class='fa fa-file-text'></i> <span>Abstract Submission</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('verif.index') }}">All Abstract</a></li>
                    <li><a href="{{ route('verif.unverif') }}">Unverif Abstract</a></li>
                    <li><a href="{{ route('verif.approved') }}">Approved Abstract</a></li>
                    <li><a href="{{ route('verif.reject') }}">Reject Abstract</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-paperclip'></i> <span>Payment</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('verif.payment')}}">Payment Masuk</a></li>
                    <li><a href="{{route('verif.payment.history')}}">Payment History</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-paperclip'></i> <span>Full Paper Submission</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">All Abstract</a></li>
                </ul>
            </li>

            <!-- For Author has been Approve Abstract -->
            @elseif(Auth::user()->id_role == 3)
            <li><a href="{{route('pay.index')}}"><i class='fa fa-file-text'></i> <span>Payment</span></a></li>
            <li><a href="{{ route('submission.index') }}"><i class='fa fa-file-text'></i> <span>Paper Submission</span></a></li>

            <!-- For Reviewer -->
            @elseif(Auth::user()->id_role > 10)
            <li><a href="{{route('review.abstrak')}}"><i class='fa fa-file-text'></i> <span>Review Abstract</span></a></li>
            @endif

            <!-- <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>{{ trans('adminlte_lang::message.multilevel') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">{{ trans('adminlte_lang::message.linklevel2') }}</a></li>
                    <li><a href="#">{{ trans('adminlte_lang::message.linklevel2') }}</a></li>
                </ul>
            </li> -->

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
