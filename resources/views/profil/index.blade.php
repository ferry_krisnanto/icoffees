@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif



	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">I-COFFEES Conference System</div>

					<div class="panel-body">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Affiliation</th>
                  <th>Email</th>
									<th>Author</th>
                  <th>Flag</th>
                </tr>
              </thead>
              <tbody>
								@foreach ($members as $member)
                <tr>
                  <td>{{$member->nama}}</td>
                  <td>{{$member->universitas}}</td>
                  <td>{{$member->email}}</td>
									<td>
											@if($member->role == 1)
											Ketua
											@elseif($member->role == 2)
											Author 1
											@elseif($member->role == 3)
											Author 2
											@elseif($member->role == 4)
											Author 3
											@elseif($member->role == 5)
											Author 4
											@endif
									</td>
                  <th></th>
                </tr>
								@endforeach
              </tbody>
            </table>

						@if($members->count() < 5)
            <div align="center">
                <a align="center" href="{{route('author.create')}}" class="btn btn-primary">Tambah Anggota</a>
            </div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
