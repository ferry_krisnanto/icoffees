@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif

<div class="row">
  <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="http://i.coffees.fh.unila.ac.id/wp-content/uploads/2018/05/I-COFFEES-Logo-300x300.png" alt="User profile picture">

            <h3 class="profile-username text-center">{{$data->name}}</h3>

            <p class="text-muted text-center">
                @if($data->id_role == 1)
                <span type="button" class="btn btn-xs bg-orange btn-flat margin">Admin</span>
                @elseif ($data->id_role ==2)
                <span type="button" class="btn btn-xs bg-purple btn-flat margin">Verificator</span>
                @elseif ($data->id_role ==3)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Author</span>
                @elseif ($data->id_role ==11)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 1</span>
                @elseif ($data->id_role ==12)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 2</span>
                @elseif ($data->id_role ==13)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 3</span>
                @elseif ($data->id_role ==14)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 4</span>
                @elseif ($data->id_role ==15)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 5</span>
                @elseif ($data->id_role ==16)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 6</span>
                @elseif ($data->id_role ==17)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 7</span>
                @elseif ($data->id_role ==18)
                <span type="button" class="btn btn-xs bg-blue btn-flat margin">Reviewer 8</span>
                @else
                <span type="button" class="btn btn-xs bg-maroon btn-flat margin">User</span>
                @endif
            </p>

            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Email :</b> <a class="pull-right"></a>
              </li>
              <li class="list-group-item">
                {{$data->email}}
              </li>
            </ul>

            <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>

      <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
							@if(Auth::user()->id_role == 0 || Auth::user()->id_role == 3)
              <li class="active"><a href="#activity" data-toggle="tab">Form Attendance of Conference</a></li>
							<li ><a href="#settings" data-toggle="tab">Change Password</a></li>
							@else
							<li class="active"><a href="#settings" data-toggle="tab">Change Password</a></li>
							@endif

            </ul>
            <div class="tab-content">
							@if(Auth::user()->id_role == 0 || Auth::user()->id_role == 3)
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post">
									<table class="table table-bordered">
			              <thead>
			                <tr>
			                  <th>Name</th>
			                  <th>University</th>
			                  <th>Faculty</th>
												<th>Author Status</th>
												<th>Attendance Conference as</th>
												<th>Dinner</th>
			                  <th>Tour</th>
												<th>Hotel</th>
			                  <th>Flag</th>
			                </tr>
			              </thead>
			              <tbody>
											@foreach ($members as $member)
			                <tr>
			                  <td>{{$member->nama}}</td>
			                  <td>{{$member->universitas}}</td>
			                  <td>{{$member->fakultas}}</td>
												<td>
														@if($member->role == 1)
														Chief Author
														@elseif($member->role == 2)
														Author 1
														@elseif($member->role == 3)
														Author 2
														@elseif($member->role == 4)
														Author 3
														@elseif($member->role == 5)
														Author 4
														@endif
												</td>
												<td>
			                    @if($member->attend == 1)
			                    Attending Conference as Presenter
			                    @elseif($member->attend == 2)
			                    Attending Conference as Public/Magister/Doctor (Non-Presenter)
			                    @elseif($member->attend == 3)
			                    Attending Conference as Students (Non-Presenter)
			                    @elseif($member->attend == 4)
			                    Not Attending Conference
			                    @endif
			                  </td>
												<td>{{$member->dinner == 0 ? 'No' : 'Yes'}}</td>
			                  <td>{{$member->tour == 0 ? 'No' : 'Yes'}}</td>
			                  <td>{{$member->hotel == 1 ? 'Yes' : 'No'}}</td>
			                  <td>
														<a class="show-modal btn btn-success" href="{{route('profil.show', $member->id)}}"><span class="glyphicon glyphicon-eye-open"></span> Show</button>
														</br>
														@if(empty($payment))
														<!-- <a class="show-modal btn btn-info" href="{{route('profil.edit', $member->id)}}"><span class="glyphicon glyphicon-eye-open"></span> Edit</button> -->
														</br>
														<a onclick="return confirm('Are you sure you want to Delete this Data?');" class="show-modal btn btn-warning" href="{{route('profil.delete', $member->id)}}"><span class="glyphicon glyphicon-eye-open"></span> Delete</button>
														@endif
												</td>
			                </tr>
											@endforeach
			              </tbody>
			            </table>
									@if(empty($payment))
										@if($members->count() < 5)
				            <div align="center">
				                <!-- <a align="center" href="{{route('author.create')}}" class="btn btn-primary">Tambah Anggota</a> -->
												<button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
												  Register Conference & Add Author Paper
												</button>
				            </div>
										@endif
									@endif
                </div>
                <!-- /.post -->
              </div>
							@endif
              <!-- /.tab-pane -->

              <div class="{{(Auth::user()->id_role == 0 || Auth::user()->id_role == 3 ? '': 'active')}} tab-pane" id="settings">
                <form class="form-horizontal" action="{{route('change.password', $data->id)}}" method="post">
                  {{csrf_field()}}
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label" >Name</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" value="{{$data->name}}" name="name" placeholder="Name" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input type="email" class="form-control" value="{{$data->email}}" name="email" placeholder="Email" disabled>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="txtNewPassword"  name="password" placeholder="Password" required minlength="8">
                      </br>
                      <input type="password" class="form-control" id="txtConfirmPassword" placeholder="Re-Type Password" onChange="checkPasswordMatch();" required>
                      <span id="divCheckPasswordMatch" ></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button id="mySelect" type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
</div>

<!-- MODAALS -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Author</h4>
      </div>
      <div class="modal-body">
				<form method="post" action="{{route('author.store')}}">
						{{csrf_field()}}
						<div class="form-group">
							<label for="exampleInputEmail1">Nama <i style="color:red;">*</i></label>
							<div class="row">
								<div class="col-md-3">
									 <input name="gelar_depan" type="text" class="form-control" placeholder="e.g : Mr. Mrs. Dr. Prof">
								 </div>
								 <div class="col-md-6">
									 <input name="nama" type="text" class="form-control" placeholder="Full Name" required>
								 </div>
								 <div class="col-md-3">
									 <input name="gelar_belakang" type="text" class="form-control" placeholder="e.g : S.H / M.H / Ph.D" >
								 </div>
							</div>
						</div>
						<div class="form-group">
							<label >Author Status <i style="color:red;">*</i></label>
							<select  name="role" class="form-control" required>
								<option value="">-- Choose --</option>
								@if(!empty($abstrak))
								<option value="1">Ketua</option>
								<option value="2">Author 1</option>
								<option value="3">Author 2</option>
								<option value="4">Author 3</option>
								<option value="5">Author 4</option>
								@endif
								<option value="5">Participants (Not Author)</option>

							</select>
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">University <i style="color:red;">*</i></label>
							<input name="universitas" type="text" class="form-control" placeholder="e.g : University of Lampung" required>
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">Dapertement/Faculty <i style="color:red;">*</i></label>
							<input name="fakultas" type="text" class="form-control" placeholder="e.g : Faculty of Lampung" required>
						</div>

						<div class="form-group">
							<label >Address <i style="color:red;">*</i></label>
							<input name="alamat" type="text" class="form-control" placeholder="" required>
						</div>

						<div class="form-group">
							<label >Address Office</label>
							<input name="alamat2" type="text" class="form-control" placeholder="">
						</div>

						<hr>
						<label >Conference Confirmation</label>
					  </br>
						<span>This data is used for calculate the Registration Fee</span>
						<p class="help-block">For Detail Price - You can view in this Link <a href="#">here</a></p>
						<hr>

						<div class="form-group">
							<label >Attendance of Conference <i style="color:red;">*</i></label>
							<select id="role" name="attend" class="form-control" onChange="showHide();" required>
								<option value="">-- Choose --</option>
								@if(!empty($abstrak))
								<option value="1">Attending Conference as Presenter</option>
								@endif
								<option value="2">Attending Conference as Public/Magister/Doctor <strong>(Non-Presenter)</strong></option>
								<option value="3">Attending Conference as Students <strong>(Non-Presenter)</strong></option>
								<option value="4">Not Attending Conference</option>
							</select>
							<p class="help-block">For Detail Price - You can view in this Link <a href="#">here</a></p>
						</div>

						<div id="ketuaShow" >
							<div class="form-group">
								<label >Phone Number</label>
								<input id="phone" name="phone" type="text" class="form-control" placeholder="" required>
							</div>

							<div class="form-group">
								<label >E-mail</label>
								<input id="email" name="email" type="email" class="form-control" placeholder="" required>
							</div>

							<label >Need stay at hotel ?<i style="color:red;">*</i></label>
							<select id="hotel" name="hotel" class="form-control" required>
								<option value="">-- Choose --</option>
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>

						</div>

						<div id="galaDinner" class="form-group">
							<label >Attendance of Gala Dinner<i style="color:red;">*</i></label>
							<select id="id_dinner" name="dinner" class="form-control" required>
								<option value="">-- Choose --</option>
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>

						<div class="form-group">
							<label >Join the Tour <i style="color:red;">*</i></label>
							<select name="tour" class="form-control" required>
								<option value="">-- Choose --</option>
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<hr>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
				</form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>

<script type="text/javascript">

function checkPasswordMatch() {
    var password = $("#txtNewPassword").val();
    var confirmPassword = $("#txtConfirmPassword").val();
    var x = document.getElementById("mySelect");

    if (password != confirmPassword){
      $("#divCheckPasswordMatch").html("Passwords do not match!");
      x.style.display = "none";
    }
    else {
      $("#divCheckPasswordMatch").html("Passwords match.");
      x.style.display = "";
    }
}

function showHide() {
    var role = $("#role").val();
    var ketua = document.getElementById("ketuaShow");
		var galaDinner = document.getElementById("galaDinner");

    if (role == 1){
      	ketua.style.display = "";
				// galaDinner.style.display = "none";
				$('#id_dinner').val(1);
				document.getElementById("phone").required = true;
				document.getElementById("email").required = true;
				document.getElementById("hotel").required = true;
    }
    else {
      	ketua.style.display = "";
				galaDinner.style.display = "";
				document.getElementById("phone").required = false;
				document.getElementById("email").required = false;
				document.getElementById("hotel").required = false;
				// $('#phone').val("");
				// $('#email').val("");
    }
}

$(document).ready(function () {
   $("#txtConfirmPassword").keyup(checkPasswordMatch);
});

</script>

@endsection
