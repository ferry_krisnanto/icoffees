@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif



	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">I-COFFEES Conference System</div>

					<div class="panel-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									 <p class="form-control-static">{{$data->gelar_depan}} {{$data->nama}} {{$data->gelar_belakang}}</p>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Author Status</label>
								<div class="col-sm-10">
									<p class="form-control-static">
										<!-- {{$data->role}} -->
										@if($data->role == 1)
										Chief Author
										@elseif($data->role == 2)
										Author 1
										@elseif($data->role == 3)
										Author 2
										@elseif($data->role == 4)
										Author 3
										@elseif($data->role == 5)
										Author 4
										@endif
									</p>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">University</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->universitas}}</p>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Dapertement/ Faculty</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->fakultas}}</p>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Address</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->alamat}}</p>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">address Office</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->alamat2}}</p>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">E-Mail</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->email}}</p>
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Phone Number</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->phone}}</p>
								</div>
							</div>

							<hr>
							<label >Conference Confirmation</label>
						  </br>
							<span>This data is used for calculate the Registration Fee</span>
							<hr>

							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Attendance of Conference</label>
								<div class="col-sm-10">
									<p class="form-control-static">
										<!-- {{$data->attend}} -->
										@if($data->attend == 1)
										Attending Conference as Presenter
										@elseif($data->attend == 2)
										Attending Conference as Public/Magister/Doctor (Non-Presenter)
										@elseif($data->attend == 3)
										Attending Conference as Students (Non-Presenter)
										@elseif($data->attend == 4)
										Not Attending Conference
										@endif
									</p>
								</div>
							</div>

							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Stay at Hotel Room</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->hotel == 1 ? 'Yes' : 'No'}}</p>
								</div>
							</div>

							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Attendance of Gala Dinner</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->dinner == 0 ? 'No' : 'Yes'}}</p>
								</div>
							</div>

							<div class="form-group">
								<label for="inputPassword3" class="col-sm-2 control-label">Join the Tour</label>
								<div class="col-sm-10">
									<p class="form-control-static">{{$data->tour == 0 ? 'No' : 'Yes'}}</p>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
