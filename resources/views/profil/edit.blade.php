@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif



	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">I-COFFEES Conference System</div>

					<div class="panel-body">
            <form method="post" action="{{route('profil.update', $data->id)}}">
                {{csrf_field()}}
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama <i style="color:red;">*</i></label>
                  <div class="row">
                    <div class="col-md-3">
                       <input value="{{$data->gelar_depan}}" name="gelar_depan" type="text" class="form-control" placeholder="e.g : Mr. Mrs. Dr. Prof">
                     </div>
                     <div class="col-md-6">
                       <input value="{{$data->nama}}" name="nama" type="text" class="form-control" placeholder="Full Name" required>
                     </div>
                     <div class="col-md-3">
                       <input value="{{$data->gelar_belakang}}" name="gelar_belakang" type="text" class="form-control" placeholder="e.g : S.H / M.H / Ph.D" >
                     </div>
                  </div>
                </div>
                <div class="form-group">
                  <label >Author Status <i style="color:red;">*</i></label>
                  <select  name="role" class="form-control" required>
                    <option value="1"
                    @if($data->role == 1)
                    selected
                    @endif
                    >Chief Author</option>

                    <option value="2"
                    @if($data->role == 2)
                    selected
                    @endif
                    >Author 1</option>

                    <option value="3"
                    @if($data->role == 3)
                    selected
                    @endif
                    >Author 2</option>

                    <option value="4"
                    @if($data->role == 4)
                    selected
                    @endif
                    >Author 3</option>
                    <option value="5"
                    @if($data->role == 5)
                    selected
                    @endif
                    >Author 4</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">University <i style="color:red;">*</i></label>
                  <input value="{{$data->universitas}}" name="universitas" type="text" class="form-control" placeholder="e.g : University of Lampung" required>
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Dapertement/Faculty <i style="color:red;">*</i></label>
                  <input value="{{$data->fakultas}}" name="fakultas" type="text" class="form-control" placeholder="e.g : Faculty of Lampung" required>
                </div>

                <div class="form-group">
                  <label >Address <i style="color:red;">*</i></label>
                  <input value="{{$data->alamat}}" name="alamat" type="text" class="form-control" placeholder="" required>
                </div>

                <div class="form-group">
                  <label >Address Office</label>
                  <input value="{{$data->alamat2}}" name="alamat2" type="text" class="form-control" placeholder="">
                </div>

                <hr>
                <label >Conference Confirmation</label>
                </br>
                <span>This data is used for calculate the Registration Fee</span>
                <hr>

                <div class="form-group">
                  <label >Attendance of Conference <i style="color:red;">*</i></label>
                  <select id="role" name="attend" class="form-control" onChange="showHide();" required>
                    <option value="">-- Choose --</option>

                    <option value="1"
                    @if($data->attend == 1)
                    selected
                    @endif
                    >Attending Conference as Presenter</option>

                    <option value="2"
                    @if($data->attend == 2)
                    selected
                    @endif
                    >Attending Conference as Public/Magister/Doctor</option>

                    <option value="3"
                    @if($data->attend == 3)
                    selected
                    @endif
                    >Attending Conference as Students</option>

                    <option value="4"
                    @if($data->attend == 4)
                    selected
                    @endif
                    >Not Attending Conference</option>
                  </select>
                </div>

                <div id="ketuaShow" style="display: none;">
                  <div class="form-group">
                    <label >Phone Number</label>
                    <input id="phone" value="{{$data->phone}}" name="phone" type="text" class="form-control" placeholder="" required>
                  </div>

                  <div class="form-group">
                    <label >E-mail</label>
                    <input id="email" value="{{$data->email}}" name="email" type="email" class="form-control" placeholder="" required>
                  </div>
                </div>

                <div id="galaDinner" class="form-group">
                  <label >Attendance of Gala Dinner<i style="color:red;">*</i></label>
                  <select id="id_dinner" name="dinner" class="form-control" required>
                    <option value="">-- Choose --</option>
                    <option value="0"
                    @if($data->dinner == 0)
                    selected
                    @endif
                    >No</option>
                    <option value="1"
                    @if($data->dinner == 1)
                    selected
                    @endif
                    >Yes</option>
                  </select>
                </div>

                <div class="form-group">
                  <label >Join the Tour <i style="color:red;">*</i></label>
                  <select name="tour" class="form-control" required>
                    <option value="">-- Choose --</option>
                    <option value="0"
                    @if($data->tour == 0)
                    selected
                    @endif
                    >No</option>

                    <option value="1"
                    @if($data->tour == 1)
                    selected
                    @endif
                    >Yes</option>
                  </select>
                </div>
                <hr>
                
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
					</div>
				</div>
			</div>
		</div>
	</div>

  <script type="text/javascript">
  function showHide() {
      var role = $("#role").val();
      var ketua = document.getElementById("ketuaShow");
      var galaDinner = document.getElementById("galaDinner");

      if (role == 1){
          ketua.style.display = "";
          galaDinner.style.display = "none";
          $('#id_dinner').val(1);


      }
      else {
          ketua.style.display = "none";
          galaDinner.style.display = "";
          document.getElementById("phone").required = false;
          document.getElementById("email").required = false;
          $('#id_dinner').val('');
          // $('#phone').val("");
          // $('#email').val("");
      }
  }
  </script>
@endsection
