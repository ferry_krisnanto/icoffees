@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Upload Abstract
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">Upload Full Paper</div>

					<div class="panel-body">
						<form method="post" action="{{route('author.set_upload')}}" enctype="multipart/form-data">
							{{ csrf_field() }}
						  <div class="form-group">
						    <label for="fileAbstract">File input</label>
						    <input type="file" id="file_paper" name="file_paper" required>
						    <p class="help-block">Upload File PDF/DOCX, Maks file size upload 2Mb</p>
						  </div>
						  <button type="submit" class="btn btn-default">Submit</button>
						</form>
				</div>
			</div>
		</div>
	</div>
@endsection
