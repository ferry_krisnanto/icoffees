@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif

	@if(!empty($paper) && $abstrak->status != 3)
	<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
			</button>
			<strong>Silahkan Bayar Biaya Registrasi terlebih dahulu Untuk Upload Paper</strong>
	</div>
	@endif

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">I-COFFEES Conference System</div>

					<div class="panel-body">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td width="25%">Title</td>
                  <td width="65%"><strong>{{ $abstrak->title }}</strong></td>
                  <td width="10%"></td>
                </tr>
                <tr >
                  <td>Author</td>
                  <td>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Affiliation/University</th>
                          <th>Author</th>
                        </tr>
                      </thead>
                      <tbody>
												@foreach ($members as $member)
				                <tr>
				                  <td>{{$member->nama}}</td>
				                  <td>{{$member->universitas}}</td>
													<td>
															@if($member->role == 1)
															Ketua
															@elseif($member->role == 2)
															Author 1
															@elseif($member->role == 3)
															Author 2
															@elseif($member->role == 4)
															Author 3
															@elseif($member->role == 5)
															Author 4
															@endif
													</td>
				                </tr>
												@endforeach
                      </tbody>
                    </table>
                  </td>
                  <td width="10%"><a href="{{route('profil')}}" class="btn btn-block btn-primary">
                    <i class="fa fa-pencil-square-o"><i>
                  </a></td>
                </tr>
                <tr>
                  <td>Abstract</td>
                  <!-- <td><a href="{{url('/file/'.$abstrak->file_name)}}" class="btn btn-primary">Download/Preview Abstract</a></td> -->
									<td><a href="{{route('submission.detail', $abstrak->id_user )}}" class="btn btn-primary">Download/Preview Abstract</a></td>
                  <td></td>
                </tr>
                <tr >
                  <td>Topic</td>
                  <td>
                    @if( $abstrak->sub_theme == 1 )
  									<p class="form-control-static">Democracy and Election</p>
  									@elseif($abstrak->sub_theme == 2)
  									<p class="form-control-static">Environmental and Natural Resources</p>
  									@elseif($abstrak->sub_theme == 3)
  									<p class="form-control-static">Modern Society and Human Security</p>
  									@elseif($abstrak->sub_theme == 4)
  									<p class="form-control-static">Business and Economic Rights</p>
  									@elseif($abstrak->sub_theme == 5)
  									<p class="form-control-static">Individual and Social Justice</p>
  									@elseif($abstrak->sub_theme == 6)
  									<p class="form-control-static">Good Governance and Public Service</p>
  									@elseif($abstrak->sub_theme == 7)
  									<p class="form-control-static">Indigenous Rights</p>
  									@elseif($abstrak->sub_theme == 8)
  									<p class="form-control-static">Woman and Children</p>
  									@endif
                  </td>
                  <td></td>
                </tr>
                <tr >
                  <td>Status</td>
                  <td>
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>Abstract</td>
                          <td>
														@if($abstrak->status == 0)
                            <button class="btn btn-danger btn-xs">Abstract Reject</button>
                            @elseif($abstrak->status == 3)
                            <button class="btn btn-success btn-xs">Abstract Approved</button>
														@elseif($abstrak->status == 2)
                            <button class="btn btn-info btn-xs">Abstract On Process Review</button>
														@elseif($abstrak->status == 1)
                            <button class="btn btn-warning btn-xs">Abstract Not Submited</button>
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <td>Payment</td>
                          <td>
														@if(empty($payment))
																<a href="#" class="btn btn-danger btn-xs">Unpaid</a>
														@elseif($payment->status == 1)
																<a href="#" class="btn btn-warning btn-xs">Please Complete the Payment</a>
														@elseif($payment->status == 2)
																<a href="#" class="btn btn-warning btn-xs">On Process Verification</a>
														@elseif($payment->status == 3)
																<a href="#" class="btn btn-success btn-xs">success</a>
														@endif
													</td>
                        </tr>
                        <tr>
                          <td>Full Paper</td>
                          <td>
														@if(empty($paper))
														<a href="#" class="btn btn-danger btn-xs">Unavailable</a>
														@endif
													</td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td></td>
                </tr>
                <tr >
                  <td>Full Paper</td>
                  <td>
										@if($abstrak->status == 3)
											@if(empty($payment))
												<a href="{{route('pay.index')}}" class="btn btn-warning btn">Complete Payment</a>
											@elseif(empty($paper))
												<!-- <a href="{{route('pay.index')}}" class="btn btn-default btn">Upload Paper</a> -->
												<!-- Small modal -->
												<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Upload Paper Now</button>

												<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
												  <div class="modal-dialog modal-sm" role="document">
												    <div class="modal-content">
															<div class="modal-header">
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												        <h4 class="modal-title" id="myModalLabel">Upload Full Paper</h4>
												      </div>
												      <div class="modal-body">
																<form method="post" action="{{route('author.set_upload')}}" enctype="multipart/form-data">
																	{{ csrf_field() }}
																  <div class="form-group">
																    <label for="fileAbstract">File input</label>
																    <input type="file" id="file_paper" name="file_paper" required>
																    <p class="help-block">Upload File PDF/DOCX, Maks file size upload 2Mb</p>
																  </div>
																  <button type="submit" class="btn btn-default">Submit</button>
																</form>
												      </div>
												    </div>
												  </div>
												</div>
											@elseif(!empty($paper->file_name))
												<a href="{{asset('/file/'.$paper->file_name )}}" class="btn btn-primary btn">Download Full Paper</a>
											@endif
										@else
											Full Paper Can't be Process Before Pay the Registration
										@endif
									</td>
                  <td width="10%"><a href="{{route('author.upload')}}" class="btn btn-block btn-primary">
                    <i class="fa fa-pencil-square-o"><i>
                  </a></td>
                </tr>
              </tbody>
            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
