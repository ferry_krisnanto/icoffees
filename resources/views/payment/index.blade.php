@extends('adminlte::layouts.app')

@section('htmlheader_title')
@endsection
@section('main-content')


@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif

<div class="alert alert-info">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
  <div>
      <p>Early Bird 15 June - 30 July 2018 </p>
  </div>
</div>

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Payment</div>
          <div class="panel-body" id="dvContents">

            <div class="row" >
              <div class="col-md-6" >
                <div class="form-group">
                  <label for="subTheme"><strong style="font-size:20px; color:red;" >I-COFFEES INVOICE</strong></label>
                  <br>

                </div>
                <hr>
                <div class="form-group">
                  <label for="subTheme"><strong>Invoice To:</strong></label>
                  <br>
                </div>
                <div class="form-group">
                  <p>Name : {{Auth::user()->name}}</p>
                  <p>Email : {{Auth::user()->email}}</p>
                </div>
                @if(!empty($payment))
                  @if($payment->status == 1)
                  <label ><strong style="font-size:16px; color:orange;" >UNPAID</strong></label>
                  @elseif($payment->status == 2)
                  <label ><strong style="font-size:16px; color:blue;" >ON PROCESS VERIFICATION</strong></label>
                  @else
                  <label ><strong style="font-size:16px; color:green;" >PAID</strong></label>
                  @endif
                  <br>
                @endif
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="subTheme"><strong>Pay To:</strong></label>
                  <br>
                </div>
                <div class="form-group">
                  <p>Bank Name : BNI</p>
                  <p>Branch : Tanjung Karang</p>
                  <p>Account Name : Nenny Dwi Ariani</p>
                  <p>Account Number : 0711326775</p>
                  <p>Confirmation : Nenny Dwi Ariani (+62 857-8970-0082)</p>
                  <p>Email : nenny.ariani@fh.unila.ac.id</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered" id="printableArea">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Detail</th>
    									<th>Price</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $i = 1;
                      $data[$members->count()] = 0;
                      $total = 0;
                    ?>
    								@foreach ($members as $member)
                    <tr>
                      <th rowspan="3">{{$i++}}</th>
                      <td rowspan="3">{{$member->nama}}
                        <br>
                        @if($member->attend == 1)
    										Attending Conference as Presenter
    										@elseif($member->attend == 2)
    										Attending Conference as Public/Magister/Doctor (Non-Presenter)
    										@elseif($member->attend == 3)
    										Attending Conference as Students (Non-Presenter)
    										@elseif($member->attend == 4)
    										Not Attending Conference
    										@endif
                      </td>
                      <td>Conference</td>
    									<td>
    											<?php

                          $A = "2018-07-30 23:59";
                          $B = date("Y-m-d h:i");

                          $A = strtotime($A); //gives value in Unix Timestamp (seconds since 1970)
                          $B = strtotime($B);

                          if ($B < $A ){
                              $early_bird = 1;

                              if ($member->attend == 1){
                                $conf = 750000;
                              }elseif ($member->attend == 2) {
                                $conf = 400000;
                              }elseif ($member->attend == 3) {
                                $conf = 200000;
                              }elseif ($member->attend == 4) {
                                $conf = 0;
                              }
                          }else{
                              $early_bird = 0;

                              if ($member->attend == 1){
                                $conf = 850000;
                              }elseif ($member->attend == 2) {
                                $conf = 500000;
                              }elseif ($member->attend == 3) {
                                $conf = 300000;
                              }elseif ($member->attend == 4) {
                                $conf = 0;
                              }
                          }

                          // Dinner Logic
                          if ($member->attend == 1) {
                              $dinner = 0;
                          }else {
                              if ($member->dinner == 1){
                                $dinner = 250000;
                              }elseif ($member->dinner == 0) {
                                $dinner = 0;
                              }
                          }

                          // Member Tour Logic
                          if ($member->tour == 1){
                            $tour = 250000;
                          }elseif ($member->tour == 0) {
                            $tour = 0;
                          }

                          // Data
                          $data[$i] = $conf + $dinner + $tour;
                          $total = $total + $data[$i];

                          ?>
                          IDR. {{ number_format( $conf , 0 , '' , ',' ) }}
    									</td>
                      <td rowspan="3">IDR. {{ number_format( $data{$i} , 0 , '' , ',' ) }}</td>
                    </tr>
                    <tr>
                      <td>Dinner</td>
                      <td>
                        @if($member->attend == 1)
                        FREE
                        @else
                        IDR. {{ number_format( $dinner , 0 , '' , ',' ) }}
                        @endif
                      </td>
                    </tr>
                    <tr>
                      <td>Tour</td>
                      <td>
                        IDR. {{ number_format( $tour , 0 , '' , ',' ) }}
                      </td>
                    </tr>
    								@endforeach
                    <td colspan="2"><strong>Total</strong></td>
                    <td></td>
                    <td></td>
                    <td>IDR. {{ number_format( $total , 0 , '' , ',' ) }}</td>

                  </tbody>
                </table>
              </div>
            </div>
          </div>

					<div class="panel-body">
            @if ($members->count() < 1)
            <div align="center">
              <p>Please Input Form Author & Conference Attendance</p><br>
              <a class="btn bg-navy" href="{{route('profil')}}"> - Click Here -</a>
            </div>
            @else
              @if(!empty($payment))
              <div align="center">
                  <input type="button" class="btn btn-info" onclick="PrintDiv();" value="Print" />
                  @if($payment->status == 1)
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    Payment Confirmation
                  </button>

                  <!-- Modal -->
                  <div align="left" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Payment Conformation</h4>
                        </div>
                        <div class="modal-body">
                          <form method="post" action="{{route('pay.upload')}}" enctype="multipart/form-data">
                              {{ csrf_field() }}
                              <div class="form-group">
                                <label for="subTheme">Name of Owner Account Bank</label>
                                <input type="text" class="form-control" name="name_user" required>
                              </div>
                              <div class="form-group">
                                <label for="subTheme">Total</label>
                                <input disabled type="number" value="{{$total}}" class="form-control" name="no_rek" required>
                              </div>

                              <div class="form-group">
                                <label for="fileAbstract">Bukti Transaksi</label>
                                <input type="file" id="fileAbstract" name="file_image" required>
                                <p class="help-block">Upload Photo</p>
                              </div>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-default">Submit</button>
                            </form>
                        </div>
                        <!-- <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div> -->
                      </div>
                    </div>
                  </div>
                  @elseif($payment->status == 2)
                  @endif

              </div>
              @else
                <form method="post" action="{{route('pay.submit')}}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div align="center">
                    <!-- <a class="btn bg-navy" href="#"> - Submit Payment - </a> -->
                    <!-- Small modal -->
                    <button type="button" class="btn bg-navy" data-toggle="modal" data-target=".bs-example-modal-sm">- Pay Now -</button>

                    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                      <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                          <input type="hidden" name="early_bird" value="{{ $early_bird }}">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Registration Fee</h4>
                          </div>
                          <div class="modal-body">
                            If you click submit button, the data and <strong>registration fee can't be edit</strong>.<br>
                            Please be Carefuly.
                          </div>
                          <!--<div class="modal-body">-->
                          <!--  Not-Availible Now,. Check Later.-->
                          <!--</div>-->
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit Pay</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              @endif
            @endif
            <a href="#"></a>
					</div>

				</div>
			</div>
		</div>
	</div>

  <script type="text/javascript">
          function PrintDiv() {
              var contents = document.getElementById("dvContents").innerHTML;
              var frame1 = document.createElement('iframe');
              frame1.name = "frame1";
              frame1.style.position = "absolute";
              frame1.style.top = "-1000000px";
              document.body.appendChild(frame1);
              var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
              frameDoc.document.open();
              frameDoc.document.write('<html><head><title>DIV Contents</title>');
              frameDoc.document.write('</head><body>');
              frameDoc.document.write(contents);
              frameDoc.document.write('</body></html>');
              frameDoc.document.close();
              setTimeout(function () {
                  window.frames["frame1"].focus();
                  window.frames["frame1"].print();
                  document.body.removeChild(frame1);
              }, 500);
              return false;
          }
      </script>

@endsection
