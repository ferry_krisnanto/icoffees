@extends('adminlte::layouts.app')

@section('htmlheader_title')
@endsection
@section('main-content')

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Payment</div>
          <div class="panel-body" id="dvContents">
            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered" id="printableArea">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Name</th>
                      <th>Detail</th>
    									<th>Price</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $i = 1;
                      $data[$members->count()] = 0;
                      $total = 0;
                    ?>
    								@foreach ($members as $member)
                    <tr>
                      <th rowspan="3">{{$i++}}</th>
                      <td rowspan="3">{{$member->nama}}
                        <br>
                        @if($member->attend == 1)
    										Attending Conference as Presenter
    										@elseif($member->attend == 2)
    										Attending Conference as Public/Magister/Doctor (Non-Presenter)
    										@elseif($member->attend == 3)
    										Attending Conference as Students (Non-Presenter)
    										@elseif($member->attend == 4)
    										Not Attending Conference
    										@endif
                      </td>
                      <td>Conference</td>
    									<td>
    											<?php

                          // $A = "2018-07-30 23:59";
                          // $B = date("Y-m-d h:i");
                          //
                          // $A = strtotime($A); //gives value in Unix Timestamp (seconds since 1970)
                          // $B = strtotime($B);

                          if ($payment->early_bird == 1 ){
                              if ($member->attend == 1){
                                $conf = 750000;
                              }elseif ($member->attend == 2) {
                                $conf = 400000;
                              }elseif ($member->attend == 3) {
                                $conf = 200000;
                              }elseif ($member->attend == 4) {
                                $conf = 0;
                              }
                          }else{
                              if ($member->attend == 1){
                                $conf = 850000;
                              }elseif ($member->attend == 2) {
                                $conf = 500000;
                              }elseif ($member->attend == 3) {
                                $conf = 300000;
                              }elseif ($member->attend == 4) {
                                $conf = 0;
                              }
                          }

                          // Dinner Logic
                          if ($member->attend == 1) {
                              $dinner = 0;
                          }else {
                              if ($member->dinner == 1){
                                $dinner = 250000;
                              }elseif ($member->dinner == 0) {
                                $dinner = 0;
                              }
                          }

                          // Member Tour Logic
                          if ($member->tour == 1){
                            $tour = 250000;
                          }elseif ($member->tour == 0) {
                            $tour = 0;
                          }

                          // Data
                          $data[$i] = $conf + $dinner + $tour;
                          $total = $total + $data[$i];

                          ?>
                          IDR. {{ number_format( $conf , 0 , '' , ',' ) }}
    									</td>
                      <td rowspan="3">IDR. {{ number_format( $data{$i} , 0 , '' , ',' ) }}</td>
                    </tr>
                    <tr>
                      <td>Dinner</td>
                      <td>
                        @if($member->attend == 1)
                        FREE
                        @else
                        IDR. {{ number_format( $dinner , 0 , '' , ',' ) }}
                        @endif
                      </td>
                    </tr>
                    <tr>
                      <td>Tour</td>
                      <td>
                        IDR. {{ number_format( $tour , 0 , '' , ',' ) }}
                      </td>
                    </tr>
    								@endforeach
                    <td colspan="2"><strong>Total</strong></td>
                    <td></td>
                    <td></td>
                    <td>IDR. {{ number_format( $total , 0 , '' , ',' ) }}</td>

                  </tbody>
                </table>
              </div>
            </div>
          </div>

					<div class="panel-body">
            <input type="button" class="btn btn-info" onclick="PrintDiv();" value="Print" />
					</div>

				</div>
			</div>
		</div>
	</div>

  <script type="text/javascript">
          function PrintDiv() {
              var contents = document.getElementById("dvContents").innerHTML;
              var frame1 = document.createElement('iframe');
              frame1.name = "frame1";
              frame1.style.position = "absolute";
              frame1.style.top = "-1000000px";
              document.body.appendChild(frame1);
              var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
              frameDoc.document.open();
              frameDoc.document.write('<html><head><title>DIV Contents</title>');
              frameDoc.document.write('</head><body>');
              frameDoc.document.write(contents);
              frameDoc.document.write('</body></html>');
              frameDoc.document.close();
              setTimeout(function () {
                  window.frames["frame1"].focus();
                  window.frames["frame1"].print();
                  document.body.removeChild(frame1);
              }, 500);
              return false;
          }
      </script>

@endsection
