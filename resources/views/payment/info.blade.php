@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

<div class="row">
  <div class="col-md-12">
      <div class="nav-tabs-custom">
        <div class="tab-content">
          <div class="active tab-pane" id="activity">
            <!-- Post -->
            <div class="post">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>University</th>
                    <th>Faculty</th>
                    <th>Author Status</th>
                    <th>Flag</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($members as $member)
                  <tr>
                    <td>{{$member->nama}}</td>
                    <td>{{$member->universitas}}</td>
                    <td>{{$member->fakultas}}</td>
                    <td>
                        @if($member->role == 1)
                        Chief Author
                        @elseif($member->role == 2)
                        Author 1
                        @elseif($member->role == 3)
                        Author 2
                        @elseif($member->role == 4)
                        Author 3
                        @elseif($member->role == 5)
                        Author 4
                        @endif
                    </td>
                    <td>
                        <a class="show-modal btn btn-success" href="{{route('profil.show', $member->id)}}"><span class="glyphicon glyphicon-eye-open"></span> Show</button>
                        </br>
                        @if(empty($payment))
                        <!-- <a class="show-modal btn btn-info" href="{{route('profil.edit', $member->id)}}"><span class="glyphicon glyphicon-eye-open"></span> Edit</button> -->
                        </br>
                        <a onclick="return confirm('Are you sure you want to Delete this Data?');" class="show-modal btn btn-warning" href="{{route('profil.delete', $member->id)}}"><span class="glyphicon glyphicon-eye-open"></span> Delete</button>
                        @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              @if(empty($payment))
                @if($members->count() < 5)
                <div align="center">
                    <!-- <a align="center" href="{{route('author.create')}}" class="btn btn-primary">Tambah Anggota</a> -->
                    <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
                      Register Conference & Add Author Paper
                    </button>
                </div>
                @endif
              @endif
            </div>
            <!-- /.post -->
          </div>
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>

</div>
@endsection
