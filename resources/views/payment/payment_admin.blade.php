@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@if ( Session::has('success') )
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
    @endif

    @if ( Session::has('error') )
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
        </button>
        <strong>{{ Session::get('error') }}</strong>
    </div>
    @endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
      <div>
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
</div>
@endif



	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Abstract</div>

					<div class="panel-body">
            <table id="example" class="table table-striped table-bordered" >
              <thead>
                <tr>
									<th>#</th>
                  <th>Nama User</th>
                  <th>Atas Nama</th>
									<th>Bukti Transaksi</th>
                  <th>Action/Status</th>
                </tr>
              </thead>
              <tbody>
								<?php $i = 1; ?>
                @foreach($payment as $data)
                <tr>
									<td>{{$i++}}</td>
                  <td>{{ $data->nama_user }}</td>
                  <td>{{ $data->name }}</td>
									<td><a href="{{asset('/transfer/'.$data->img_trans)}}" target="blank" class="btn btn-primary">Show Image</a></td>
                  <td>
                    @if($data->status == 2)
                    <a href="{{route('verif.payment.confirm', $data->id_user)}}" class="btn btn-success" onclick="return confirm('Are you sure you want to Confir this Payment?');">Confirm</a>
                    @elseif($data->status == 3)
                    <span class="btn btn-success">Success</span>
                    @endif
										<a href="{{route('verif.payment.info', $data->id_user)}}" class="btn btn-info">Detail</a>
										<a href="{{route('verif.payment.invoice', $data->id_user)}}" class="btn bg-maroon btn-flat">Invoice</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
document.addEventListener("contextmenu", function(e){
  e.preventDefault();
}, false);
</script>

@endsection
