<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;

class AdminController extends Controller
{
    public function test()
    {
       $data = DB::table('users')
              ->leftjoin('abstract', 'users.id', '=', 'abstract.id_user')
              ->where('status', null)
              ->get();

        return json_decode($data);
    }

    public function mail_send()
    {
      $pesan = "Attention to all Participants who have not submitted an Abstract for International Conference please upload that, at http://i-coffees.id/conference befor 23:59 today (July 10, 2018)." ;
      $user = "Admin I-COFFEES";
      Mail::send('email', ['nama' => $user, 'pesan' => $pesan], function ($message) use ($user)
      {
          $message->subject('Notice - Deadline Abstract');
          $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
          $message->to([
              // 'nabellaronasahati@gmail.com',
              // 'r.masterwijaya@gmail.com',
              // 'rizkypradanapanjaitan@gmail.com',
              // 'susaanszn@gmail.com',
              // 'syahputraadriawansh@gmail.com',
              // 'syt250105@gmail.com',
              // 'theodora.saputri@yahoo.com',
              'ferrykrisnanto312@gmail.com'
            ]);
      });

      return "sukses";
    }
}
