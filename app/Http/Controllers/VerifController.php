<?php

namespace App\Http\Controllers;

use DB;
use Uuid;
use Mail;
use Session;
use Auth;
use App\User;
use Illuminate\Http\Request;

class VerifController extends Controller
{
    public function index()
    {
        $datas = DB::table('abstract')
            ->join('users', 'users.id', '=', 'abstract.id_user')
            ->select('abstract.*', 'users.name', 'users.email')
            ->where('abstract.status', '!=', 1)
            ->orderBy('created_at', 'ASC')
            ->get();
        // return json_encode($datas);
        return view('abstract.show', compact('datas'));
    }

    public function unverif()
    {
        $datas = DB::table('abstract')
            ->join('users', 'users.id', '=', 'abstract.id_user')
            ->select('abstract.*', 'users.name', 'users.email')
            ->where('abstract.status', 2)
            ->orderBy('created_at', 'ASC')
            ->get();
        // return json_encode($datas);
        $name = "Belum Di Setujui";
        return view('abstract.show', compact('datas', 'name'));
    }

    public function approved()
    {
      $datas = DB::table('abstract')
          ->join('users', 'users.id', '=', 'abstract.id_user')
          ->select('abstract.*', 'users.name', 'users.email')
          ->where('abstract.status', 3)
          ->orderBy('created_at', 'ASC')
          ->get();
      // return json_encode($datas);
      $name = "Sudah Di setujui";
      return view('abstract.show', compact('datas', 'name'));
    }

    public function reject()
    {
      $datas = DB::table('abstract')
          ->join('users', 'users.id', '=', 'abstract.id_user')
          ->select('abstract.*', 'users.name', 'users.email')
          ->where('abstract.status', 0)
          ->orderBy('created_at', 'ASC')
          ->get();
      // return json_encode($datas);
      $name = "Ditolak";
      return view('abstract.show', compact('datas', 'name'));
    }

    public function show($id)
    {
        $abstract = DB::table('abstract')->where('id', $id)->first();
        return view('abstract.index', compact('abstract'));
    }

    public function actionApprove($id)
    {
        $true = DB::table('abstract')
            ->where('id', $id)
            ->update(['status' => 3]);

        $abstract = DB::table('abstract')->where('id', $id)->first();
        $user     = DB::table('users')->where('id', $abstract->id_user)->first();

        $update   = DB::table('users')
            ->where('id', $user->id)
            ->update(['id_role' => 3]);

        if ($true) {
            $abstract = DB::table('abstract')->where('id', $id)->first();
            $user = DB::table('users')->where('id', $abstract->id_user)->first();
            $pesan = "Congratulations, Your Abstract was Accepted. Please login and check our system for further information. http://i-coffees.id/conference";
            try{
                Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan], function ($message) use ($user)
                {
                    $message->subject('Abstract Notification - Aprroved');
                    $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
                    $message->to($user->email);
                });

                Session::flash('success', 'Your Data has successfully Update');
            }
            catch (Exception $e){
                // return response (['status' => false,'errors' => $e->getMessage()]);
                Session::flash('error', 'Your Data Cannot be Updated '. $e);
            }
        }else {
            Session::flash('error', 'Your Data Cannot be Updated');
        }
        return redirect()->back();
    }

    public function actionReject($id)
    {
        $true = DB::table('abstract')
            ->where('id', $id)
            ->update(['status' => 0]);

        if ($true) {
            $abstract = DB::table('abstract')->where('id', $id)->first();
            $user = DB::table('users')->where('id', $abstract->id_user)->first();
            $pesan = "Sorry Your Abstract We Can not Accepted. Please try it on another occasion";
            try{
                Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan], function ($message) use ($user)
                {
                    $message->subject('Abstract Notification');
                    $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
                    $message->to($user->email);
                });
                Session::flash('success', 'Your Data has successfully Update');
            }
            catch (Exception $e){
                // return response (['status' => false,'errors' => $e->getMessage()]);
                Session::flash('error', 'Your Data Cannot be Updated '. $e);
            }
        }else {
            Session::flash('error', 'Your Data Cannot be Updated');
        }
        return redirect()->back();
    }

    public function user()
    {
        $user = DB::table('users')->orderBy('created_at', 'DESC')->get();
        // return json_encode($user);
        return view('peserta.index', compact('user'));
    }

    public function userDetail($id)
    {
        $abstrak = DB::table('abstract')
          ->where('id_user', $id)
          ->first();

        $members = DB::table('anggota')
            ->where('id_user', $id)
            ->OrderBy('role', 'ASC')
            ->get();

        $paper = DB::table('paper')
            ->where('id_user', $id)
            ->first();

        if (empty($abstrak)) {
            return redirect()->route('submission.detail', $id);
        }else {
            return view('submission.admin', compact('abstrak', 'members', 'paper'));
        }
    }

    public function user_belum_abstrak()
    {
        $user = DB::table('users')
             ->leftjoin('abstract', 'users.id', '=', 'abstract.id_user')
             ->where('status', null)
             ->select('users.*')
             ->get();
        // return json_encode($user);
        return view('peserta.index', compact('user'));
    }

    public function user_belum_submit()
    {
      $user = DB::table('users')
           ->leftjoin('abstract', 'users.id', '=', 'abstract.id_user')
           ->where('status', 1)
           ->select('users.*')
           ->get();
        // return json_encode($user);
        return view('peserta.index', compact('user'));
    }

    public function payment()
    {
        $payment = DB::table('payment')
            ->leftjoin('users', 'payment.id_user', '=', 'users.id')
            ->select('payment.*', 'users.name as nama_user')
            ->where('payment.status', 2)
            ->get();

        // return json_encode($payment);
        return view('payment.payment_admin', compact('payment'));
    }

    public function payment_confirm($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        $pesan = "Your Payment has been verified. You can upload Full Paper before deadline.";
        Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan], function ($message) use ($user)
        {
            $message->subject('Payment Notification');
            $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
            $message->to($user->email);
        });

        $payment = DB::table('payment')
            ->where('id_user', $id)
            ->update([
                'status' => 3
            ]);
      Session::flash('success', 'Your Data has successfully Update');
      return redirect()->back();
    }

    public function payment_history()
    {
      $payment = DB::table('payment')
          ->leftjoin('users', 'payment.id_user', '=', 'users.id')
          ->select('payment.*', 'users.name as nama_user')
          ->where('payment.status', 3)
          ->get();

        // return json_encode($payment);
        return view('payment.payment_admin', compact('payment'));
    }

    public function payment_invoice($id)
    {
        $members = DB::table('anggota')->where('id_user', $id)->get();
        $payment = DB::table('payment')->where('id_user', $id)->first();

        return view('payment.invoice', compact('payment', 'members'));
    }

    public function payment_info($uid)
    {
        $data = DB::table('users')->where('id', $uid)->first();
        $members = DB::table('anggota')->where('id_user', $uid)->get();
        $abstrak = DB::table('abstract')->where('id_user', $uid)->first();
        $payment = DB::table('payment')->where('id_user', $uid)->first();
        // return json_encode($abstrak);
        return view('payment.info', compact('data', 'members', 'payment', 'abstrak'));
    }

    public function report()
    {
        $user = DB::table('anggota')
            ->leftJoin('users', 'users.id', '=', 'anggota.id_user')
            ->leftJoin('abstract', 'abstract.id_user', '=', 'anggota.id_user')
            ->select('users.email as emailuser', 'anggota.*', 'abstract.title')
            ->get();
        // return json_encode($user);
        return view('report', compact('user'));
    }

    public function submitMessage(Request $request)
    {
      $this->validate($request, [
	        'file' => 'file|max:2000|mimes:pdf,doc,docx'
	    ]);
	    if ($request->hasFile('file')) {
	        if($request->file('file')->isValid()) {
	            try {
	                $file = $request->file('file');
	                $name = Uuid::generate(4) . '-tiket.' . $file->getClientOriginalExtension();

	                $berhasil = $request->file('file')->move("tiket", $name);
	            } catch (Illuminate\Filesystem\FileNotFoundException $e) {

	            }
	        }
	    }
      else {
        $name = NULL;
      }

      DB::table('message')->insert([
        'id_user' => $request->id_user,
        'pesan' => $request->pesan,
        'file' => $name
      ]);

      $user = DB::table('users')->where('id', $request->id_user)->first();
      $pesan = "You have new notification on your system, please check the system. https://i-coffees.id/conference/author/submission/$request->id_user .";
      if ($name != NULL) {
        $pesan = $pesan . "and you get a file attachment, you can akses this link.https://i-coffees.id/conference/tiket/$name ";
      }

      Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan], function ($message) use ($user)
      {
          $message->subject('Message Notification - Do not reply this email');
          $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
          $message->to($user->email);
      });

      return redirect()->back();
    }
}
