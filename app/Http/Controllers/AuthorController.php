<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Uuid;
use File;
use Mail;
use Session;
class AuthorController extends Controller
{
    public function index()
    {
        $id = Auth::user()->id;
        $abstrak = DB::table('abstract')
          ->where('id_user', $id)
          ->first();

        $members = DB::table('anggota')
            ->where('id_user', $id)
            ->OrderBy('role', 'ASC')
            ->get();

        $payment = DB::table('payment')
            ->where('id_user', $id)
            ->first();

        $paper = DB::table('paper')
            ->where('id_user', $id)
            ->first();

        if (empty($abstrak)) {
            return redirect()->route('submission.detail', $id);
        }else {
            return view('submission.index', compact('abstrak', 'members', 'paper', 'payment'));
        }
    }

    public function show($id)
    {
    	$abstract = DB::table('abstract')->where('id_user', $id)->first();
      $tiket = DB::table('message')->where('id_user', $id)->get();
    	return view('abstract.index', compact('abstract', 'tiket'));
    }

    // public function profil()
    // {
    //     $members = DB::table('anggota')
    //         ->where('id_user', Auth::user()->id)
    //         ->OrderBy('role', 'ASC')
    //         ->get();
    //
    //    return view('profil.index', compact('members'));
    // }

    public function create()
    {
        return view('profil.create');
    }

    public function store(Request $request)
    {
      DB::table('anggota')->insert([
          'id' => Uuid::generate(4),
          'id_user' => Auth::user()->id,
          'gelar_depan' => $request->gelar_depan,
          'gelar_belakang' => $request->gelar_belakang,
          'nama' => $request->nama,
          'universitas' => $request->universitas,
          'fakultas' => $request->fakultas,
          'email' => $request->email,
          'phone' => $request->phone ,
          'alamat' => $request->alamat,
          'alamat2' => $request->alamat2,
          'role' => $request->role,
          'attend' => $request->attend,
          'dinner' => $request->dinner,
          'tour'  => $request->tour,
          'hotel'  => $request->hotel
      ]);
      Session::flash('success', 'Your Data has successfully Added');
      return redirect()->back();
    }

    public function showAnggota($id)
    {
      $data = DB::table('anggota')->where('id', $id)->first();
      return view('profil.show', compact('data'));
    }

    public function editAnggota($id)
    {
      $data = DB::table('anggota')->where('id', $id)->first();
      return view('profil.edit', compact('data'));
    }

    public function updateAnggota($id, Request $request)
    {
       DB::table('anggota')->where('id', $id)
            ->update([
              'gelar_depan' => $request->gelar_depan,
              'gelar_belakang' => $request->gelar_belakang,
              'nama' => $request->nama,
              'universitas' => $request->universitas,
              'fakultas' => $request->fakultas,
              'email' => $request->email,
              'phone' => $request->phone ,
              'alamat' => $request->alamat,
              'alamat2' => $request->alamat2,
              'role' => $request->role,
              'attend' => $request->attend,
              'dinner' => $request->dinner,
              'tour'  => $request->tour,
              'hotel'  => $request->hotel
            ]);

            Session::flash('success', 'Your Data has successfully Updated');
            return redirect()->route('profil.show');
    }

    public function deleteAnggota($id)
    {
        $hapus = DB::table('anggota')->where('id', $id)->delete();
        if ($hapus) {
          Session::flash('success', 'Your Data has successfully deleted');
          return redirect()->back();
        }else {
          Session::flash('error', 'Your Data has unsuccessfully deleted');
          return redirect()->back();
        }
    }

    public function upload()
    {
        return view('fullpaper.create');
    }

    public function set_upload(Request $request)
    {
        $this->validate($request, [
            'file_paper' => 'required|file|max:10000|mimes:pdf,doc,docx'
        ]);

        $paper = DB::table('paper')->where('id_user', Auth::user()->id)->first();

        if (!empty($paper)){
          $file = File::delete('file/' . $paper->file_name);
          if($file){
              DB::table('paper')->where('id_user', Auth::user()->id)->delete();
              // Session::flash('success', 'Your Data has successfully Deleted');
              // return redirect()->route('abstract.create');
          }else {
              Session::flash('error', 'Your Data Cannot be Deleted');
              return redirect()->route('fullpaper.index');
          }
        }

        if ($request->hasFile('file_paper')) {
            if($request->file('file_paper')->isValid()) {
                try {
                    $file = $request->file('file_paper');
                    $name = UUid::generate(4) . '-fullpaper.' . $file->getClientOriginalExtension();

                    $berhasil = $request->file('file_paper')->move("file", $name);
                } catch (Illuminate\Filesystem\FileNotFoundException $e) {

                }
            }
        }

        if($berhasil){
          DB::table('paper')->insert([
              'id'        => Uuid::generate(4),
              'id_user'   => Auth::user()->id,
              'file_name' => $name
          ]);
          Session::flash('success', 'Your Data has successfully imported');
        return redirect()->back();

        }else{
          Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
          return redirect()->back();
        }
    }

    public function payment()
    {

        // return "Payment under-maintance";
        $members = DB::table('anggota')->where('id_user', Auth::user()->id)->get();
        $payment = DB::table('payment')->where('id_user', Auth::user()->id)
              ->first();

        return view('payment.index', compact('payment', 'members'));
    }

    public function payment_submit(Request $request)
    {
       $pay = DB::table('payment')->insert([
          // 'id' => Uuid::generate(4),
          'id_user' => Auth::user()->id,
          'status'  => 1,
          'early_bird'  => $request->early_bird
       ]);

       if ($pay) {
         Session::flash('success', 'Your Data Has Been Save');
         return redirect()->back();
       }else {
         Session::flash('error', 'Your Data Error, Please Call Admin');
         return redirect()->back();
       }


    }

    public function confirm_pay (Request $request)
    {

        $this->validate($request, [
            'file_image' => 'required|image|max:2000|mimes:jpg,jpeg,png'
        ]);

        if ($request->hasFile('file_image')) {
            if($request->file('file_image')->isValid()) {
                try {
                    $file = $request->file('file_image');
                    $name = UUid::generate(4) . '-transfer.' . $file->getClientOriginalExtension();

                    $berhasil = $request->file('file_image')->move("transfer", $name);
                } catch (Illuminate\Filesystem\FileNotFoundException $e) {

                }
            }
        }

        if($berhasil){
            DB::table('payment')
                ->where('id_user', Auth::user()->id)
                ->update([
                  'name' => $request->name_user,
                  'img_trans' => $name,
                  'status' => 2
                ]);

            $user = DB::table('users')->where('id', Auth::user()->id)->first();
            $pesan = "Your Payment was Submitted. We will give futher information should.";
            Mail::send('email', ['nama' => Auth::user()->name, 'pesan' => $pesan], function ($message) use ($user)
            {
                $message->subject('Payment Notification');
                $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
                $message->to(Auth::user()->email);
            });

            $pesan = "Pemberitahuan untuk user dengan Nama : $user->name dan E-mail : $user->email sudah upload bukti pembayaran, bisa dilihat di https://i-coffees.id/conference/transfer/$name" ;
            Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan], function ($message) use ($user)
            {
                $message->subject('Payment - Need Confirmation'. $user->name);
                $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
                $message->to([
                    'i.coffees@fh.unila.ac.id',
                    'ferrykrisnanto312@gmail.com',
                    'rudi.natamiharja@fh.unila.ac.id',
                    'rudi.natamiharja@gmail.com',
                    'rudy.1981@fh.unila.ac.id'
                  ]);
            });

            Session::flash('success', 'Your Data Has Been Save');
            return redirect()->back();

        }else{
          Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid IMAGE file..!!');
          return redirect()->back();
        }


    }
}
