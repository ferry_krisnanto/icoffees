<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Uuid;
use File;
use Mail;
use Session;
use Illuminate\Http\Request;

class UploadAbstractController extends Controller
{

    public function store(Request $request)
    {
    	$this->validate($request, [
	        'fileAbstract' => 'required|file|max:2000|mimes:pdf,doc,docx'
	    ]);

	    if ($request->hasFile('fileAbstract')) {
	        if($request->file('fileAbstract')->isValid()) {
	            try {
	                $file = $request->file('fileAbstract');
	                $name = UUid::generate(4) . '-icoffees.' . $file->getClientOriginalExtension();

	                $berhasil = $request->file('fileAbstract')->move("file", $name);
	            } catch (Illuminate\Filesystem\FileNotFoundException $e) {

	            }
	        }
	    }

	    if($berhasil){
	    	DB::table('abstract')->insert([
			    'id' 		=> UUid::generate(4),
			    'file_name'	=> $name,
			    'id_user' 	=> $request->id_user,
			    'title'		=> $request->abstactTitle,
			    'sub_theme'	=> $request->subTheme,
			    'status'	=> 1
			]);
	    	Session::flash('success', 'Your Data has successfully save');
			  return redirect()->back();

	    }else{
	    	Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid pdf/docx file..!!');
	    	return redirect()->back();
	    }
    }

    public function delete($id)
    {
        $data = DB::table('abstract')
            ->where('id', $id)
            ->first();

        if (!empty($data)){
            if ($data->status == 1) {
                $file = File::delete('file/' . $data->file_name);
                if($file){
                    DB::table('abstract')->where('id', $id)->delete();
                    Session::flash('success', 'Your Data has successfully Deleted');
                    return redirect()->route('abstract.create');
                }else {
                    Session::flash('error', 'Your Data Cannot be Deleted');
                    return redirect()->route('submission.detail', $id);
                }
            }

        }
        Session::flash('error', 'Cannot be Process');
        return redirect()->route('submission.index');
    }

    public function submit($id)
    {
        $true = DB::table('abstract')
            ->where('id', $id)
            ->update(['status' => 2]);

        if ($true) {
            $abstract = DB::table('abstract')->where('id', $id)->first();
            $user = DB::table('users')->where('id', $abstract->id_user)->first();
            $pesan = "Thank you for your submition at International Conference on Funfamental Rights organized by Faculty of Law, University of Lampung (Unila).
            We will give futher information should.";
            try{
                Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan], function ($message) use ($user)
                {
                    $message->subject('Submitted Abstract');
                    $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
                    $message->to($user->email);
                });

                $pesan = "Pemberitahuan untuk user dengan Nama : $user->name dan E-mail : $user->email sudah upload abstrak, bisa dilihat di https://i-coffees.id/conference/file/$abstract->file_name" ;
                Mail::send('email', ['nama' => $user->name, 'pesan' => $pesan], function ($message) use ($user)
                {
                    $message->subject('Upload Abstrak - '. $user->name);
                    $message->from('donotreply@i-icoffees.id', 'Admin ICOFFEES');
                    $message->to([
                        'i.coffees@fh.unila.ac.id',
                        'ferrykrisnanto312@gmail.com',
                        'rudi.natamiharja@fh.unila.ac.id',
                        'rudi.natamiharja@gmail.com',
                        'rudy.1981@fh.unila.ac.id'
                      ]);
                });
                Session::flash('success', 'Your Abstract has successfully submitted');
            }
            catch (Exception $e){
                // return response (['status' => false,'errors' => $e->getMessage()]);
                Session::flash('error', 'Your Abstract cannot be submitted '. $e);
            }
        }else {
            Session::flash('error', 'Your Abstract cannot be submitted');
        }
        return redirect()->back();
    }
}
