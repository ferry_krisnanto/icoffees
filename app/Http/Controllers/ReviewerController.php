<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class ReviewerController extends Controller
{
    public function abstrak()
    {
        $id_theme = Auth::user()->id_role - 10;
        $datas = DB::table('abstract')
            ->join('users', 'users.id', '=', 'abstract.id_user')
            ->select('abstract.*', 'users.name', 'users.email')
            ->where('abstract.sub_theme', $id_theme)
            ->where('abstract.status', '!=', 1)
            ->orderBy('created_at', 'ASC')->paginate(25);
        // return json_encode($datas);
        return view('abstract.show', compact('datas'));
    }
}
