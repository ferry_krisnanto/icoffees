<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Mail;
use Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiket = DB::table('message')->where('id_user', Auth::user()->id)->get();
        return view('vendor.adminlte.home', compact('tiket'));
    }

    public function profil()
    {
        $uid = Auth::user()->id;
        $data = DB::table('users')->where('id', $uid)->first();
        $members = DB::table('anggota')->where('id_user', $uid)->get();
        $abstrak = DB::table('abstract')->where('id_user', $uid)->first();
        $payment = DB::table('payment')->where('id_user', $uid)->first();
        // return json_encode($abstrak);
        return view('profil.profil', compact('data', 'members', 'payment', 'abstrak'));
    }

    public function changePassword($id, Request $request)
    {
        DB::table('users')->where('id', $id)
              ->update([
                'name'     =>  $request->name,
                'password' =>  bcrypt($request->password)
              ]);

        Session::flash('success', 'Your Password has been change');
        return redirect()->back();
    }

    public function test(Request $request)
    {
      try{
          Mail::send('email', ['nama' => $request->nama, 'pesan' => $request->pesan], function ($message) use ($request)
          {
              $message->subject($request->judul);
              $message->from('donotreply@kiddy.com', 'Kiddy');
              $message->to($request->email);
          });
          return back()->with('alert-success','Berhasil Kirim Email');
      }
      catch (Exception $e){
          return response (['status' => false,'errors' => $e->getMessage()]);
      }
    }

    public function dump_email()
    {
      $data = DB::table('abstract')
          ->join('users', 'abstract.id_user', '=', 'users.id')
          ->select('users.email','users.name' )
          ->orderBy('abstract.created_at', 'ASC')
          ->get();

      $pesan = "Yang kami Hormati, Calon Presenter I-Coffees 2018

Terkait dengan Abstrak Notification yang awalnya dijadwalkan pada 23 Juli 2018, kami informasikan bahwa jadwal tersebut diundur lagi sampai 25 Juli 2018.

Kami selaku panitia memohon maaf atas ketidaknyamanan ini.

Terima kasih";

      for ($i=101; $i < $data->count(); $i++) {
            Mail::send('email', ['nama' => $data[$i]->name, 'pesan' => $pesan], function ($message) use ($data, $i)
            {
                $message->subject('Abstract Notification - Extended');
                $message->from('i.coffees@fh.unila.ac.id', 'I-COFFEES');
                $message->to($data[$i]->email);
            });
      }
      return "sukses $i";
        // return json_encode($data->count());
    }
}
