<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class Reviewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check()) {
          $role = Auth::user()->id_role;
          if ( $role > 10 && $role < 20) {
              return $next($request);
          }
          return redirect('home');
          // return json_encode($role);
        }
        return redirect('login');
    }
}
