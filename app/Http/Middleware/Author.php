<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Author
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check()) {
            if (Auth::user()->id_role == 3) {
                return $next($request);
            }
            return redirect('home');
        }
        return redirect('login');
    }
}
